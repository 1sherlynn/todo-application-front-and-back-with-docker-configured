
# Todo Application Frontend + Backend + Docker Config 

- Frontend: https://todo-frontend-app.herokuapp.com/
- Backend: https://api-server-todo.herokuapp.com/todos

### Run Container
```bash
$ docker-compose up --build
```

### Configuration for localhost 
 - Currently configured to production API database: https://api-server-todo.herokuapp.com
 - To change to localhost: toggle and uncomment at **/client/src/store.js**
```bash
 const client = axios.create({ //all axios can be used, shown in axios documentation
     baseURL: 'https://api-server-todo.herokuapp.com',
     // baseURL: 'https://todo-app-react-node.herokuapp.com',
     // baseURL: 'http://localhost:5000', 
     responseType: 'json'
    }
  );

```

### If using localhost 
- Need to create Database and Table 
```bash
$ $ docker-compose up --build
$ docker-compose exec postgres psql -U postgres -c “create database todos”
$ \c todos
$ CREATE TABLE todos (
ID SERIAL PRIMARY KEY,
text VARCHAR(1000),
marked BOOL
);
```